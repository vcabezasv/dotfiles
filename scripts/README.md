# Scripts

Interpreted scripts in POSIX shell, Bash, Perl, Ruby, Python, etc.

## Install

Create symbolic link:

```sh
mkdir -p ~/.local/bin/ && ln -sf "$PWD" ~/.local/bin/scripts
```

Add to path:

```
export PATH=~/.local/bin/scripts:~/.local/bin:$PATH
```


