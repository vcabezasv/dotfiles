#!/bin/sh -e
# Install eclipse

set -o errexit
set -o nounset

if [ ! -d ~/.jenv ]; then
    echo "jenv not installed"
    exit 1
fi

ECLIPSE_HOME="/usr/eclipse"
ECLIPSE_EXECUTABLE="$ECLIPSE_HOME/eclipse"
ECLIPSE_VERSION="2023-03"
ECLIPSE_URL="http://mirror.dkm.cz/eclipse/technology/epp/downloads/release/${ECLIPSE_VERSION}/R/eclipse-jee-${ECLIPSE_VERSION}-R-linux-gtk-x86_64.tar.gz"

if [ ! -f $ECLIPSE_EXECUTABLE ]; then
    echo "Installing eclipse"
    \wget -qO- ${ECLIPSE_URL} | sudo tar -C /usr/ -zxv
    sudo ln -sf ${ECLIPSE_EXECUTABLE} /usr/bin/eclipse
fi

echo "Eclipse $(grep version $ECLIPSE_HOME/.eclipseproduct) installed"
