#[ -e "$HOME/.sh_exports" ] && source "$HOME/.sh_exports"

if [ -d ~/.local/bin ]; then
    export PATH="$HOME/.local/bin:$PATH"
    if [ -L ~/.local/bin/scripts ]; then
        export PATH="$HOME/.local/bin/scripts:$PATH"
    fi
fi


## Python
# ----------
if [ -z "$(command -v pyenv)" ]; then
    if [ ! -z "$(command -v $HOME/.pyenv/bin/pyenv)" ]; then
        export PYENV_ROOT="$HOME/.pyenv"
        export PATH="$PYENV_ROOT/bin:$PATH"
    fi
    if command -v pyenv >/dev/null 2>&1; then
        eval "$(pyenv init --path)"
        if command -v pyenv-virtualenv >/dev/null 2>&1; then
            eval "$(pyenv virtualenv-init -)"
        fi
    fi
fi


