# -*-sh-*-

#set -x
# command debug
#{ set +x; } 2>/dev/null


# Load our dotfiles like ~/.bash_prompt, etc...
#   ~/.extra can be used for settings you don’t want to commit,
#   Use it to configure your PATH, thus it being first in line.
for file in ~/.bash_{extra,exports,aliases}; do
    [ -r "$file" ] && source "$file"
done
unset file
[[ -e "$HOME/.sh_functions" ]] && source "$HOME/.sh_functions"

##
## gotta tune that bash_history…
##

# Enable history expansion with space
# E.g. typing !!<space> will replace the !! with your last command
#bind Space:magic-space
bind -f ~/.inputrc

shopt -s cmdhist               # Save multi-line commands as one command
shopt -s nocaseglob;           # Case-insensitive globbing (used in pathname expansion)
shopt -s cdspell;              # Correct spelling errors in arguments supplied to cd
shopt -s dirspell 2> /dev/null # Autocorrect on directory names to match a glob.
shopt -s globstar 2> /dev/null # Turn on recursive globbing (enables ** to recurse all directories)

##
## Completion…
##

if [[ -n "$ZSH_VERSION" ]]; then  # quit now if in zsh
    return 1 2> /dev/null || exit 1;
fi;

# Enable tab completion for `g` by marking it as an alias for `git`
if type __git_complete &> /dev/null; then
    __git_complete g __git_main
fi;

# Add tab completion for `defaults read|write NSGlobalDomain`
# You could just use `-g` instead, but I like being explicit
complete -W "NSGlobalDomain" defaults

# Automatically trim long paths in the prompt (requires Bash 4.x)
PROMPT_DIRTRIM=2

## Powerline
# ----------
if xset q &> /dev/null; then
    if [ -f `which powerline-daemon` ]; then
        powerline-daemon -q
        POWERLINE_BASH_CONTINUATION=1
        POWERLINE_BASH_SELECT=1
    fi
    if [ -f /usr/share/powerline/bindings/bash/powerline.sh ]; then
        source /usr/share/powerline/bindings/bash/powerline.sh
    fi
fi

# tmux
if xset q &> /dev/null; then
    declare -r wm_name=$(wmctrl -m | grep Name | awk '{print tolower($2)}')
    if xset q &>/dev/null && command -v tmux &>/dev/null && [[ $wm_name = "openbox" ]] && ! tmux ls &>/dev/null && [ -z "$(tmux ls | grep mainob)" ]; then
        # tmux attach -t default ||
        tmux new -s mainob
    fi
fi

## Neofetch
# ----------
if xset q &>/dev/null && [[ -f `which neofetch` ]] && [[ -z "$TMUX" ]]; then
    neofetch
fi

if [ -f ~/.sh_profile ]; then
    . ~/.sh_profile
fi

