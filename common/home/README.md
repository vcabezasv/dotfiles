## Bash config

### Functions

- md(): Create a new directory and enter it
```sh
md dir/dir/dir
```

- f(): find shorthand
```sh
f filename
```

- la(): List all files, long format, colorized, permissions in octal
```sh
```

- log(): git commit browser. needs fzf
```sh
```

- clone(): clone repository and install dependencies (nodejs)
```sh
```

- server(): Start an HTTP server from a directory, optionally specifying the port
```sh
```

- cp_p(): Copy w/ progress
```sh
```

- gz(): get gzipped size
```sh
```

- whois(): whois a domain or a URL
```sh
```

- csvpreview(): preview csv files. source: http://stackoverflow.com/questions/1875305/command-line-csv-viewer
```sh
```

- extract(): Extract archives - use: extract <file>
```sh
```

- camerausedby(): who is using the laptop's iSight camera?
```sh
```

- gifify(): animated gifs from any video
```sh
```

- webmify(): turn that video into webm.
```sh
```

- nullify(): direct it all to /dev/null
```sh
```

- code(): visual studio code. a la `subl`
```sh
```

- dataurl(): Create a data URL from an image (works for other file types too, if you tweak the Content-Type afterwards)
```sh
```

- httpcompression(): Test if HTTP compression (RFC 2616 + SDCH) is enabled for a given URL.
```sh
```

- gurl(): Gzip-enabled `curl`
```sh
```

- json(): Syntax-highlight JSON strings or files
```sh
```

- digga(): All the dig info
```sh
```

- escape(): Escape UTF-8 characters into their 3-byte format
```sh
```

- unidecode(): Decode \x{ABCD}-style Unicode escape sequences
```sh
```

- codepoint(): Get a character’s Unicode code point
```sh
```

- cdls(): perform 'ls' after 'cd' if successful.
```sh
```

- myusb(): Need to figure out which drive your usb is assigned?
```sh
```

- cdl(): cd to the directory a symbolically linked file is in.
```sh
```

- cdff(): cd to the dir that a file is found in.
```sh
```

- mkcd(): create directory and enter it


