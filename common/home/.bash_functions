# -*-sh-*-
# declare -F

# Create a new directory and enter it
function mkcd() {
    mkdir -p "$@" \
        && builtin cd "$@"
}

# perform 'ls' after 'cd' if successful.
function cdls() {
    builtin cd "$*"
    local -r RESULT=$?
    if [ "$RESULT" -eq 0 ]; then
        ls
    fi
}

# find shorthand
function findfile() {
    builtin find . -name "$1" 2>&1 |
        builtin grep -v 'Permission denied'
}

# clone repo and install dependencies
function cloneyarn() {
    git clone --depth=1 $1
    cd $(basename ${1%.*})
    yarn install
}

# git config
function gitconfig {
    local -r name="$1"
    local -r email="$2"
    git config user.name "$name"
    git config user.email "$email"
    git config --global core.autocrlf input
    git config -l
}

# Copy w/ progress
function rsyncp () {
    rsync -WavP --human-readable --progress $1 $2
}

# get gzipped size
function gzipsize() {
    local -r orig_size=$(cat "$1" | wc -c)
    local -r gzip_size=$(gzip -c "$1" | wc -c)
    echo "orig size    (bytes): $orig_size"
    echo "gzipped size (bytes): $gzip_size"
}

# whois a domain or a URL
function whoisurl() {
  local DOMAIN=$(echo "$1" | awk -F/ '{print $3}') # get domain from URL
  if [ -z $DOMAIN ] ; then
    DOMAIN="$1"
  fi
  readonly $DOMAIN
  echo "Getting whois record for: $DOMAIN ..."

  # avoid recursion
          # this is the best whois server
                          # strip extra fluff
  /usr/bin/whois -h whois.internic.net $DOMAIN | sed '/NOTICE:/q'
}

# Extract archives - use: extract <file>
# Based on http://dotfiles.org/~pseup/.bashrc
function extract() {
  if [ -f "$1" ] ; then
    local -r filename=$(basename "$1")
    local -r foldername="${filename%%.*}"
    local -r fullpath=`perl -e 'use Cwd "abs_path";print abs_path(shift)' "$1"`
    local didfolderexist=false
    if [ -d "$foldername" ]; then
      didfolderexist=true
      read -p "$foldername already exists, do you want to overwrite it? (y/n) " -n 1
      echo
      if [[ $REPLY =~ ^[Nn]$ ]]; then
        return
      fi
    fi
    mkdir -p "$foldername" && cd "$foldername"
    case $1 in
      *.tar.bz2) tar xjf "$fullpath" ;;
      *.tar.gz) tar xzf "$fullpath" ;;
      *.tar.xz) tar Jxvf "$fullpath" ;;
      *.tar.Z) tar xzf "$fullpath" ;;
      *.tar) tar xf "$fullpath" ;;
      *.taz) tar xzf "$fullpath" ;;
      *.tb2) tar xjf "$fullpath" ;;
      *.tbz) tar xjf "$fullpath" ;;
      *.tbz2) tar xjf "$fullpath" ;;
      *.tgz) tar xzf "$fullpath" ;;
      *.txz) tar Jxvf "$fullpath" ;;
      *.zip) unzip "$fullpath" ;;
      *) echo "'$1' cannot be extracted via extract()" && cd .. && ! $didfolderexist && rm -r "$foldername" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

# who is using the laptop's iSight camera?
function camerausedby() {
  echo "Checking to see who is using the iSight camera… 📷"
  local -r usedby=$(lsof | grep -w "AppleCamera\|USBVDC\|iSight" |
               awk '{printf $2"\n"}' | xargs ps)
  echo -e "Recent camera uses:\n$usedby"
}


# animated gifs from any video
# from alex sexton   gist.github.com/SlexAxton/4989674
function gifify() {
    if [[ -n "$1" ]]; then
        if [[ $2 == '--good' ]]; then
            ffmpeg -i "$1" -r 10 -vcodec png out-static-%05d.png
            time convert -verbose +dither -layers Optimize -resize 900x900\> out-static*.png  GIF:- | gifsicle --colors 128 --delay=5 --loop --optimize=3 --multifile - > "$1.gif"
            rm out-static*.png
        else
            ffmpeg -i "$1" -s 600x400 -pix_fmt rgb24 -r 10 -f gif - | gifsicle --optimize=3 --delay=3 > "$1.gif"
        fi
    else
        echo "proper usage: gifify <input_movie.mov>. You DO need to include extension."
    fi
}

# turn that video into webm.
# brew reinstall ffmpeg --with-libvpx
function webmify() {
    ffmpeg -i "$1" -vcodec libvpx -acodec libvorbis -isync -copyts -aq 80 -threads 3 -qmax 30 -y "$2" "$1.webm"
}

# direct it all to /dev/null
function nullify() {
    "$@" >/dev/null 2>&1
}

# Create a data URL from an image (works for other file types too, if you tweak the Content-Type afterwards)
function dataurl() {
    echo "data:image/${1##*.};base64,$(openssl base64 -in "$1")" | tr -d '\n'
}

# Test if HTTP compression (RFC 2616 + SDCH) is enabled for a given URL.
# Send a fake UA string for sites that sniff it instead of using the Accept-Encoding header. (Looking at you, ajax.googleapis.com!)
function httpcompression() {
    local -r encoding="$(curl -LIs -H 'User-Agent: Mozilla/5 Gecko' -H 'Accept-Encoding: gzip,deflate,compress,sdch' "$1" | grep '^Content-Encoding:')" && echo "$1 is encoded using ${encoding#* }" || echo "$1 is not using any encoding"
}

# Gzip-enabled `curl`
function gurl() {
    curl -sH "Accept-Encoding: gzip" "$@" | gunzip
}

# All the dig info
function digga() {
    local -r DOMAIN="$1"
    local RECORD="$2"
    if [[ -z $RECORD ]]; then
        RECORD="any"
    fi
    readonly "$RECORD"

    dig +nocmd "$DOMAIN" "$RECORD" +multiline +noall +answer
}

# reverse DNS
function rdns() {
    local -r IP="$1"
    dig +nocmd +noall +answer -x "$IP"
}

# Escape UTF-8 characters into their 3-byte format
function escape() {
    printf "\\\x%s" $(printf "$@" | xxd -p -c1 -u)
    echo # newline
}

# Decode \x{ABCD}-style Unicode escape sequences
function unidecode() {
  perl -e "binmode(STDOUT, ':utf8'); print \"$@\""
  echo # newline
}

# Get a character’s Unicode code point
function codepoint() {
    perl -e "use utf8; print sprintf('U+%04X', ord(\"$@\"))"
    echo # newline
}

function myusb() {
    local -a usb_array=();
    while read -r -d $'n'
    do
        usb_array+=("$REPLY");
    done < <(find /dev/disk/by-path/ -type l -iname *usb*scsi* -not -iname *usb*scsi*part* -print0 | xargs -0 -iD readlink -f D | cut -c 8) \
        && for usb in "${usb_array[@]}"
    do
        echo "USB drive assigned to sd$usb"
    done
}

# cd to the directory a symbolically linked file is in.
function cdl() {
    if [ "x$1" = "x" ] ; then
        echo "Missing Arg"
    elif [ -L "$1" ] ; then
        local -r link=`/bin/ls -l $1 | tr -s ' ' | awk '{print $NF}' `
        if [ "x$link" = "x" ] ; then
            echo "Failed to get link"
            return
        fi
        local -r dirName_=`dirname $link`
        cd "$dirName_"
    else
        echo "$1 is not a symbolic link"
    fi
    return
}

# cd to the dir that a file is found in.
function cdff() {
    local -r filename=`find . -name $1 | grep -iv "Permission Denied" | head -1`
    if [ "xx${filename}xx" != "xxxx" ] ; then
        local -r dirname=${filename%/*}
        if [ -d $dirname ] ; then
            cd $dirname
        fi
    fi
}


## ANSI escape codes - colors
NC="\033[0m"
BLACK="\033[0;30m"
RED="\033[0;31m"
GREEN="\033[0;32m"
BROWN_ORANGE="\033[0;33m"
BLUE="\033[0;34m"
PURPLE="\033[0;35m"
CYAN="\033[0;36m"
LIGHT_GRAY="\033[0;37m"

DARK_GRAY="\033[1;30m"
LIGHT_RED="\033[1;31m"
LIGHT_GREEN="\033[1;32m"
YELLOW="\033[1;33m"
LIGHT_BLUE="\033[1;34m"
LIGHT_PURPLE="\033[1;35m"
LIGHT_CYAN="\033[1;36m"
WHITE="\033[1;37m"

function bf_loggerok() {
    echo -e "${LIGHT_GREEN}[  OK  ]${NC} ${1}"
}

function bf_loggerinfo() {
    echo -e "${YELLOW}[ INFO ]${NC} ${1}"
}

function bf_loggerwarn() {
    echo -e "${BROWN_ORANGE}[ WARN ]${NC} ${1}"
}

function bf_loggerfailed() {
    echo -e "${LIGHT_RED}[FAILED]${NC} ${1}"
}


function tmuxnew() {

    local -r SESSION_NAME="$1"

    # check session
    tmux has -t "$SESSION_NAME" 2>/dev/null
    if [ $? != 0 ]; then
        tmux new -s "$SESSION_NAME" -n win1 -d
        tmux neww -t "$SESSION_NAME" -n win2 -d
        tmux neww -t "$SESSION_NAME" -n win3 -d
        tmux neww -t "$SESSION_NAME" -n win4 -d

        tmux send -t "$SESSION_NAME":win1 "echo win1" ENTER
        tmux send -t "$SESSION_NAME":win2 "echo win2" ENTER
        tmux send -t "$SESSION_NAME":win3 "echo win3" ENTER
        tmux send -t "$SESSION_NAME":win4 "echo win4" ENTER
    fi

    # attach to session
    if [ -z "$TMUX" ]; then
        tmux a -t "$SESSION_NAME"
    else
        tmux switchc -t "$SESSION_NAME"
    fi
}

function tmuxnew2() {

    local -r SESSION_NAME="$1"

    # check session
    tmux has -t "$SESSION_NAME" 2>/dev/null
    if [ $? != 0 ]; then
        tmux new -d -s "$SESSION_NAME" -n win1
        tmux splitw -t "$SESSION_NAME":win1  -d
        tmux splitw -t "$SESSION_NAME":win1  -d
        tmux neww   -t "$SESSION_NAME" -n win2 -d
        tmux splitw -t "$SESSION_NAME":win2  -d
        tmux splitw -t "$SESSION_NAME":win2  -d

        # tmux send -t "$SESSION_NAME":win1 "echo win1" Enter
        # tmux send -t "$SESSION_NAME":win1.2 "echo pane2" Enter
        # tmux send -t "$SESSION_NAME":win1.3 "echo pane3" Enter
        # tmux send -t "$SESSION_NAME":win2 "echo win2" Enter
        # tmux send -t "$SESSION_NAME":win2.2 "echo pane2" Enter
        # tmux send -t "$SESSION_NAME":win2.3 "echo pane3" Enter
    fi

    # attch to session
    if [ -z "$TMUX" ]; then
        tmux a -t "$SESSION_NAME"
    else
        tmux switchc -t "$SESSION_NAME"
    fi
}

function tmuxkillsession() {

    local -r SESSION_NAME="$1"

    # check session
    tmux has -t $SESSION_NAME 2>/dev/null
    if [ $? == 0 ]; then
        tmux kill-session -t $SESSION_NAME
    else
        echo "$SESSION_NAME not exist"
    fi
}

function tmuxkillserver() {
    tmux kill-server
}
