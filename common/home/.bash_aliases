# -*-sh-*-

[ -e "$HOME/.sh_aliases" ] && source "$HOME/.sh_aliases"

## Colorize the grep command output for ease of use (good for log files)##
alias grepc='grep --color=auto'
alias grepi='grep --color=auto -i'
alias egrepc='egrep --color=auto'
alias fgrepc='fgrep --color=auto'
