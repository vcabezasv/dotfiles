#!/usr/bin/env bash

# Set the brightness level with a zenity dialog box

# Dependencies: `xbacklight` or `light-git`, `zenity`

# Prefer the `light` package, use `xbacklight` if `light` not found
# if [[ $(which light) == *"/light"* ]]
# then
#     lvl=$(light -G)
# else
#     lvl=$(xbacklight -get)
# fi
lvl=$(xrandr --verbose | grep -m 1 -i brightness | cut -f2 -d ' ')

# Lets round the float result
# lvl=$(echo "($lvl+0.5)/1" | bc)
lvl=$(echo "($lvl*100)/1" | bc)

lvl=$(zenity --scale --value ${lvl} --title "Brightness" --text "Set brightness level")

# if [[ $(which light) == *"/light"* ]]
# then
#     light -S ${lvl}
# else
#     xbacklight -set ${lvl}
# fi

lvl=$(echo "scale=2; ($lvl/100.0)" | bc | awk '{printf "%.2f", $1}')
outputname=$(xrandr -q | grep " connected" | cut -f1 -d ' ')
xrandr --output ${outputname} --brightness ${lvl}
