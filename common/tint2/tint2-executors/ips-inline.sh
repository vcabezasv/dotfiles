#!/usr/bin/env bash

# This script displays external and internal IP address in a single row
# Depends on `bind-tools` (Arch) or `dnsutils` (Debian)

# Author: Piotr Miller
# e-mail: nwg.piotr@gmail.com
# Website: http://nwg.pl
# Project: https://github.com/nwg-piotr/tint2-executors
# License: GPL3

#w=$(dig +short myip.opendns.com @resolver1.opendns.com)
w=$(curl -s https://myip.dnsomatic.com)
l=$(ip addr | grep 'state UP' -A2 | tail -n1 | awk '{print $2}' | cut -f1  -d'/')
dnsw=$(dig -x $w +noall +answer | awk '{print $NF}')

isvpn=$(nmcli connection show --active | egrep -i "vpn|wireguard|tun")

local iconPrefix=""
if [[ -n ${isvpn} ]]; then
    iconPrefix=""
else
    iconPrefix=""
fi

echo -e "${iconPrefix} WAN: ${w} ${dnsw} LAN: ${l}"
