# Victor's dotfiles

This repo contains my configuration files

## Paths

Create symbolic links:

```
sh common/setup-all
```

## Config

Modify .bashrc:

Change prompt

```sh
PS1='${debian_chroot:+($debian_chroot)}\u \W \$ '
```

```sh
if [ -f ~/.bash_profile ]; then
    . ~/.bash_profile
fi
```

## crontab

```
*/5 * * * * $HOME/.config/battery/.battnotif
```

## authinfo

- Gmail

```
machine imap.gmail.com login user password pass port imaps
machine smtp.gmail.com login user password pass port 587
```

- office365

```
machine outlook.office365.com login user password pass port imaps
machine smtp.office365.com    login user password pass port 587
```
